import { resolve } from "styled-jsx/css";
import MovieCard from "../components/MovieCard";
import styles from "@/app/styles/common.module.css"

const Movie = async () => {
  //loading spinner timeout
  await new Promise(resolve => setTimeout(resolve, 2000));
  //url
  const url = process.env.RAPID_KEY
  const options = {
    method: 'GET',
    headers: {
      'X-RapidAPI-Key': 'ee1c7353a9msh0613498fa9bd59dp16e144jsnc926d2fe74a5',
      'X-RapidAPI-Host': 'netflix54.p.rapidapi.com'
    }
  };
  const res = await fetch(url, options);
  const data = await res.json();
  const main_data = data.titles
  //console.log(main_data);

  return (
    <>
      <section className={styles.movieSection}>
        <div className={styles.container}>
          <h1>Series and Movies</h1>
          <div className={styles.card_section}>

            {
              main_data.map((curElem) => {
                return <MovieCard key={curElem.id} {...curElem} />
              })
            }
          </div>
        </div>
      </section>
    </>
  )
}

export default Movie