'use client'
import styles from "@/app/contact/contact.module.css"
import { Mulish } from 'next/font/google';
import { useState } from "react";


const mulish = Mulish({
    weight: ['300', '400', '500', '600', '700', '800', '900'],
    subsets: ['latin'],
    display: 'swap'
})

const ContactForm = () => {
    const [status, setStatus] = useState(null);
    const [user, setUseer] = useState({
        username: "",
        email: "",
        phonenumber: "",
        message: ""
    })

    const handleChange = (e) => {
        const name=e.target.name;
        const value=e.target.value;

        setUseer((prevUser)=>({
            ...prevUser,
            [name]:value
        }))
    }
    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await fetch('/api/contact', {
                method:'POST',
                headers:{"Content_Type":"application/json"},
                body: JSON.stringify({
                    username:user.username,
                    email:user.email,
                    phone:user.phone,
                    message:user.message
                })
            })
            // Set the status based on the response from the API route
            if (response.status === 200) {
                setUser({
                    username: "",
                    email: "",
                    phone: "",
                    message: ""
                })
                setStatus('success');
            } else {
                setStatus('error');
            }

        }catch (e) {
            console.log(e)
        }

    }

    return (
        <form className={styles.contact_form} onSubmit={handleSubmit}>
            <div className={styles.input_field}>
                <label htmlFor="username" className={styles.label}> Enter your Name :
                    <input required value={user.username} onChange={handleChange} className={mulish.className} type="text"
                     name="username" id="username" placeholder="enter your name"
                    
                      />
                </label>
            </div>

            <div className={styles.input_field}>
                <label htmlFor="email" className={styles.label}> Enter your Email :
                    <input required value={user.email} onChange={handleChange} className={mulish.className} type="text" name="email" id="email" placeholder="enter your email" />
                </label>
            </div>

            <div className={styles.input_field}>
                <label htmlFor="phonenumber" className={styles.label}> Enter your phone number :
                <input required value={user.phonenumber} onChange={handleChange} className={mulish.className} type="number" name="phonenumber" id="phonenumber" placeholder="enter your phone number" />
                </label>
            </div>

            <div className={styles.input_field}>
                <label htmlFor="message" className={styles.label}> Enter Message :
                    <textarea required value={user.message} onChange={handleChange}
                    className={mulish.className} rows={5} type="text" name="message" id="message" placeholder="enter your message" />
                </label>
            </div>
            <div>
            {status === 'success' && <p className={styles.success_msg}>Thank you for your message!</p>}
                {status === 'error' && <p className={styles.error_msg}>There was an error submitting your message. Please try again.</p>}

                <button className={mulish.className} type="submit">Send message</button>
            </div>
        </form>
        
    )
}

export default ContactForm