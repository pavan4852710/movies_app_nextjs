import styles from "@/app/contact/contact.module.css"
import { MdEmail } from "react-icons/md";
import { MdChat } from "react-icons/md";
import { RiCommunityFill } from "react-icons/ri";

import Link from "next/link"
const ContactCard = () => {
  return (
    <div className={styles.section}>
      <div className={styles.container}>
        <div className={styles.grid}>
        <div className={styles.grid_card}>
        <i><MdEmail/></i>
        <h2>Email</h2>
        <p>Monday to friday expected</p>
            <Link href="/"><p> Send Email</p></Link>
        </div>

        <div className={styles.grid_card}>
        <i><MdChat/></i>
        <h2>Live Chat</h2>
        <p>Monday to friday expected</p>
            <Link href="/"><p> chat now</p></Link>
        </div>

        <div className={styles.grid_card}>
        <i><RiCommunityFill/></i>
        <h2>Community forun</h2>
        <p>Monday to friday expected</p>
            <Link href="/"><p> Ask the Community</p></Link>
        </div>
        </div>
      </div>
    </div>
  )
}

export default ContactCard