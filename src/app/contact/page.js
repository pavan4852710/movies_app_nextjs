import ContactCard from "../components/ContactCard"
import ContactForm from "../components/ContactForm"
import styles from './contact.module.css'

const page = () => {
  return (
    <>
      <div className={styles.Container}>
      <h1>Contact Us</h1>
      <ContactCard/>
      
      <section className={styles.contact_section}>
          <h2>we would love to her <span>from you</span></h2>
          <ContactForm/>
      </section>
      </div>
      {/*<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3807.0568709372396!2d78.50862472493517!3d17.409058183481786!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9999b3256e1b%3A0xa7c7a59fd930855f!2sRamnagar%20Gundu%2C%20Adikmet%2C%20Hyderabad%2C%20Telangana!5e0!3m2!1sen!2sin!4v1712919592350!5m2!1sen!2sin" 
        width={600} height={450} style={{border:0}} 
        className={styles.mapping}
        allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
      */}

    </>
  )
}

export default page