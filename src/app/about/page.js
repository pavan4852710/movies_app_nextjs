import Herosection from "../components/Herosection"

const page = () => {
  return (
    <Herosection title={"About us ...our story"} imageUrl={"/about1.svg"}/>
  )
}

export default page